Source: pygeoif
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3,
               python3-pytest,
               python3-setuptools,
               python3-typing-extensions
Standards-Version: 4.6.2
Homepage: https://github.com/cleder/pygeoif
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/python-team/packages/pygeoif.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pygeoif

Package: python3-pygeoif
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
Description: basic implementation of the __geo_interface__ (Python 3)
 PyGeoIf provides a GeoJSON-like protocol for geo-spatial (GIS) vector data.
 .
 So when you want to write your own geospatial library with support for this
 protocol you may use pygeoif as a starting point and build your functionality
 on top of it
 .
 You may think of pygeoif as a ‘shapely ultralight’ which lets you construct
 geometries and perform very basic operations like reading and writing
 geometries from/to WKT, constructing line strings out of points, polygons from
 linear rings, multi polygons from polygons, etc. It was inspired by shapely and
 implements the geometries in a way that when you are familiar with shapely you
 feel right at home with pygeoif.
